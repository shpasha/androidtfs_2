package xyz.shpasha.androidtfs_2;

import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.design.chip.Chip;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Station> stations;
    private TagLayout tagLayout, tagLayout1;
    private MyOnClickListener buttonClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        stations = new ArrayList<>();
        stations.add(new Station("Бунинская аллея", "#A1B3D4"));
        stations.add(new Station("Фили", "#00BFFF"));
        stations.add(new Station("Новокузнецкая", "#2DBE2C"));
        stations.add(new Station("Калужская", "#ED9121"));
        stations.add(new Station("Октябрьская", "#8D5B2D"));
        stations.add(new Station("Водный стадион", "#2DBE2C"));
        stations.add(new Station("Солнцево", "#FFD702"));
        stations.add(new Station("Парк победы", "#0078BE"));
        stations.add(new Station("Академическая", "#ED9121"));
        stations.add(new Station("Ленинский проспект", "#ED9121"));
        stations.add(new Station("Южная", "#999999"));
        stations.add(new Station("Менделеевская", "#999999"));
        stations.add(new Station("Лубянка", "#EF161E"));
        stations.add(new Station("Комсомольская", "#EF161E"));
        stations.add(new Station("Беговая", "#800080"));
        stations.add(new Station("ВДНХ", "#ED9121"));
        stations.add(new Station("Новокосино", "#FFD702"));
        stations.add(new Station("Пушкинская", "#800080"));
        stations.add(new Station("Сокольники", "#EF161E"));
        stations.add(new Station("Выставочная", "#00BFFF"));

        tagLayout = findViewById(R.id.tagLayout);
        tagLayout1 = findViewById(R.id.tagLayout1);

        buttonClickListener = new MyOnClickListener();

        for (Station s : stations) {
            GradientDrawable drawable = new GradientDrawable();
            drawable.setShape(GradientDrawable.OVAL);
            drawable.setColor(s.getColor());
            Chip chip = new Chip(this);
            chip.setCheckable(false);
            chip.setChipText(s.getName());
            chip.setCloseIconEnabled(true);
            chip.setChipIcon(drawable);
            chip.setChipIconSize(30);
            chip.setTag(1);
            chip.setOnCloseIconClickListener(buttonClickListener);
            ViewGroup.MarginLayoutParams chipLayoutParam = new ViewGroup.MarginLayoutParams( ViewGroup.LayoutParams.WRAP_CONTENT,  ViewGroup.LayoutParams.WRAP_CONTENT);
            chipLayoutParam.setMargins(10, 10, 10,10);
            chip.setLayoutParams(chipLayoutParam);
            tagLayout.addView(chip);
        }
    }
    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if ((int)v.getTag() == 1) {
                tagLayout.removeView(v);
                tagLayout1.addView(v);
                v.setTag(0);
            } else {
                tagLayout1.removeView(v);
                tagLayout.addView(v);
                v.setTag(1);
            }
        }
    }
}
