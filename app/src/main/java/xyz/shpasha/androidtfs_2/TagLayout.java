package xyz.shpasha.androidtfs_2;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

public class TagLayout extends ViewGroup {

    private int deviceWidth;
    public TagLayout(Context context) {
        this(context, null, 0);
    }

    public TagLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TagLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        final Display display = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        Point deviceDisplay = new Point();
        display.getSize(deviceDisplay);
        deviceWidth = deviceDisplay.x;
    }


    @Override
    protected void dispatchDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor( Color.RED );
        paint.setStrokeWidth(4);
        canvas.drawLine(0, getHeight(), getWidth(), getHeight(), paint);
        super.dispatchDraw(canvas);
    }


    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        int count = getChildCount();

        int left, top, maxHeight;

        int layoutWidth = getMeasuredWidth() - getPaddingRight() - getPaddingLeft();
        int layoutHeight = getMeasuredHeight() - getPaddingBottom() - getPaddingTop();
        int numInRow = 0;

        maxHeight = 0;
        left = getPaddingLeft();
        top = getPaddingTop();

        for (int i = 0; i < count; i++) {
            View child = getChildAt(i);

            if (child.getVisibility() == GONE)
                return;

            MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();
            left += lp.leftMargin;

            child.measure(MeasureSpec.makeMeasureSpec(layoutWidth, MeasureSpec.AT_MOST), MeasureSpec.makeMeasureSpec(layoutHeight, MeasureSpec.AT_MOST));

            int curWidth = child.getMeasuredWidth();
            int curHeight = child.getMeasuredHeight();

            if (left + curWidth + lp.rightMargin > layoutWidth) {

                int right = getMeasuredWidth() - getPaddingRight();;
                for (int j = 1; j <= numInRow; j++) {
                    View prevChild = getChildAt(i - j);
                    int prevWidth = prevChild.getMeasuredWidth();
                    int prevHeight = prevChild.getMeasuredHeight();
                    MarginLayoutParams prevLP = (MarginLayoutParams)prevChild.getLayoutParams();
                    right -= prevLP.rightMargin;

                    prevChild.layout(right - prevWidth,  top + prevLP.topMargin, right, top + prevLP.topMargin + prevHeight);
                    right -= prevWidth + prevLP.leftMargin;
                }

                left = getPaddingLeft() + lp.leftMargin;
                top += maxHeight;
                maxHeight = 0;
                numInRow = 0;
            }

            if (maxHeight < curHeight + lp.topMargin + lp.bottomMargin)
                maxHeight = curHeight + lp.topMargin + lp.bottomMargin;

            numInRow++;
            left += curWidth + lp.rightMargin;
        }

        if (numInRow != 0) {
            int right = getMeasuredWidth() - getPaddingRight();
            for (int j = 1; j <= numInRow; j++) {
                View prevChild = getChildAt(count - j);
                int prevWidth = prevChild.getMeasuredWidth();
                int prevHeight = prevChild.getMeasuredHeight();
                MarginLayoutParams prevLP = (MarginLayoutParams)prevChild.getLayoutParams();
                right -= prevLP.rightMargin;
                prevChild.layout(right - prevWidth,  top + prevLP.topMargin, right, top + prevLP.topMargin + prevHeight);
                right -= prevWidth + prevLP.leftMargin;
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int count = getChildCount();
        int viewGroupHeight = 0;
        int viewGroupWidth = 0;
        int maxHeightInRow = 0;
        int left = 0;
        int childState = 0;

        for (int i = 0; i < count; i++) {
            final View child = getChildAt(i);

            if (child.getVisibility() == GONE)
                continue;

            MarginLayoutParams lp = (MarginLayoutParams) child.getLayoutParams();
            measureChild(child, widthMeasureSpec, heightMeasureSpec);

            int childWidthWithMargins = lp.leftMargin + child.getMeasuredWidth() + lp.rightMargin;
            int childHeightWithMargins = lp.topMargin + lp.bottomMargin + child.getMeasuredHeight();

            if (left + childWidthWithMargins > deviceWidth) {
                viewGroupWidth = Math.max(left, viewGroupWidth);
                viewGroupHeight += maxHeightInRow;
                left = 0;
                maxHeightInRow = childHeightWithMargins;
            } else {
                maxHeightInRow = Math.max(maxHeightInRow, childHeightWithMargins);
            }

            left += childWidthWithMargins;
            childState = combineMeasuredStates(childState, child.getMeasuredState());
        }
        viewGroupHeight += maxHeightInRow;

        setMeasuredDimension(resolveSizeAndState(viewGroupWidth, widthMeasureSpec, childState), resolveSizeAndState(viewGroupHeight, heightMeasureSpec, childState << MEASURED_HEIGHT_STATE_SHIFT));
    }
}