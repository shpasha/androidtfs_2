package xyz.shpasha.androidtfs_2;

import android.graphics.Color;


public class Station {

    private String name = "";
    private Integer color = 0;


    public Station(String name, String color) {
        this.name = name;
        this.color = Color.parseColor(color);
    }

    public String getName() {
        return name;
    }

    public int getColor() {
        return color;
    }


}
